class Person(val firstName:String, val lastName: String, val age:Int){
  def this(f:String, l:String) = this(f,l,34)

}

object Book  extends  App {
    println("I am here for you.")
    val name = "Emmanuel"
    println(f"Hello, $name")
    val p = new Person("John","Doe", 34)
    val p1 = new Person("Emma","Doe")
    println(f"${p.firstName}, ${p.lastName}")
    println(f"${p1.firstName}, ${p1.lastName}")

}
