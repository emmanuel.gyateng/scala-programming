object CelsiusToFahrenheit {

  def celsiusToFahrenheit(celsius:Int): Int ={
    (celsius* 9/5) +32

  }

  def main(args: Array[String]): Unit ={

    println(celsiusToFahrenheit(25))
  }

}
