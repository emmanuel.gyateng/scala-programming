import scala.language.postfixOps

object DatesClass {

  def main (args: Array[String]) :Unit= {
    println(dateConverterClass("01/02/15"))
  }

  def dateConverterClass(date:String): String = {
    val dateList = date.split("/")
    var day = dateList(0)
    var month = dateList(1).toInt
    var monthInWords :String = ""
    val year = f"20${dateList(2)}"
    day(1) match{
      case '1' if (day(0)=='0' || day(1)=='1') => day=f"${day(1)}st"
      case '2' if (day(0)=='0' && day(1)=='2') => day=f"${day(1)}nd"
      case '3' if (day(0)=='0' && day(1)=='3') => day=f"${day(1)}rd"
      case i if (day(0)=='2' && day(1)=='1') => day=f"${day}st"
      case i if (day(0)=='2' && day(1)=='2') => day=f"${day}nd"
      case i if (day(0)=='2' && day(1)=='3') => day=f"${day}rd"
      case _ => day=f"${day}th"
    }
    month match{
      case  1 => monthInWords= "January"
      case  2 => monthInWords= "February"
      case  3 => monthInWords= "March"
      case 4 => monthInWords= "April"
      case  5 => monthInWords= "May"
      case  6 => monthInWords= "June"
      case  7 => monthInWords= "July"
      case  8 => monthInWords= "August"
      case  9 => monthInWords= "September"
      case  10 => monthInWords= "October"
      case  11 => monthInWords= "November"
      case  12 => monthInWords= "December"
    }
    f"$day $monthInWords $year"
  }
}
