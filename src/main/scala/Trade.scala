class Trade(val id :String, val Symbol :String, val quantity:Int, v:Double){
  private var _price = v //initialPrice is constructor parameter
  def price: Double = _price //getter method
  def price_=(v: Double): Unit = { if (v >= 0) _price = v }
  def value:Double = {
    quantity*v
  }
  override def toString: String = f"id: $id, Symbol:$Symbol, Quantity: $quantity, Price:$price"
}

object Trade extends  App {
    val trade = new Trade(id = "23D", Symbol = "Af", quantity = 2, v = 13.6)
    println(trade)
    trade.price = -23
    println(trade.price)
    println(trade.value)
}

